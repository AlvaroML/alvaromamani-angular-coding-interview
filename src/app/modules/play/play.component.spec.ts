import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayComponent } from './play.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';

fdescribe('PlayComponent', () => {
  let component: PlayComponent;
  let fixture: ComponentFixture<PlayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayComponent ],
      imports: [AppRoutingModule, HttpClientModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayComponent);
    component = fixture.componentInstance;
    // component.getQuestionsSubscription = 
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // xfit('Should display an error message when the API does not work correctly', ()=> {
  //   // const mockCall = spyOn(component, 'getQuestionsSubscription').and.returnValue(Observable.throw({status: 404}));

  //   // expect()
  // });
});
