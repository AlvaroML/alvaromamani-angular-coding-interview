import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { QuestionsService } from '../../core/services/questions.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { QuestionModel } from '../../core/state/question.model';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlayComponent implements OnInit {
  displayErrorMessage = false;
  errorMessage: string = '';
  questions$ = this.questionsService.questions$;
  gettingQuestions$ = this.questionsService.gettingQuestions$;
  getQuestionsSubscription: Subscription = this.route.queryParams
    .pipe(switchMap(params =>
      this.questionsService.getQuestions({
        type: params.type,
        amount: params.amount,
        difficulty: params.difficulty
      })
    )).subscribe(
      (_)=> {
      }, (error)=> {
        this.displayErrorMessage = true;
        this.errorMessage = `There was an error trying to fetch the data.`;
        console.error('There was an error trying to fetch the data', error);
      }
    );

  constructor(
    private readonly route: ActivatedRoute,
    private readonly questionsService: QuestionsService,
  ) { }

  ngOnInit(): void {
    this.questionsService.questions$.subscribe((result)=> {
      const allAnswered = Array.isArray(result) && result.length > 0 && result.every((data) => data.selectedId);
      if(allAnswered) {
        const countAnswers = result.map((question)=> {
          const correctAnswer = question.answers.find(answer => answer.isCorrect)?._id === question.selectedId;
          return correctAnswer;
        });
        const countCorrectAnswers = countAnswers.filter(isCorrect => isCorrect).length;
        const countIncorrectAnswers = result.length - countCorrectAnswers;        
      }
    });
  }

  onAnswerClicked(questionId: QuestionModel['_id'], answerSelected: string): void {
    this.questionsService.selectAnswer(questionId, answerSelected);
  }

}
